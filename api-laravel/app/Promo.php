<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Promo extends Model

{
    protected $table = 'promos';

    public function students() {

        return $this->hasMany('App\Student', 'promo_id');
    }


}
