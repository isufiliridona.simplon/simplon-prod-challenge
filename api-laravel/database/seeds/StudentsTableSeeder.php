<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('students')->insert([
            'name' => 'Liridona',
            'surname' => 'Isufi',
            'email' => 'liridona.isufi@outlook.fr',
            'url' => 'https://www.simplonlyon.fr/promo9/lisufi/',
            'promo_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('students')->insert([
            'name' => 'Melissa',
            'surname' => 'Galvan',
            'email' => 'mel.gvn69@gmail.com',
            'url' => 'https://www.simplonlyon.fr/promo9/mgalvan/',
            'promo_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('students')->insert([
            'name' => 'Benoit',
            'surname' => 'Jouve',
            'email' => 'benoit.juve69@gmail.com',
            'url' => 'https://www.simplonlyon.fr/promo9/bjouve/',
            'promo_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('students')->insert([
            'name' => 'Thomas',
            'surname' => 'Sebastianelli',
            'email' => 'thomas.seb@gmail.com',
            'url' => 'https://www.simplonlyon.fr/promo7/tsebastianelli/',
            'promo_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('students')->insert([
            'name' => 'François',
            'surname' => 'Kong',
            'email' => 'kong.francois@gmail.com',
            'url' => 'https://www.simplonlyon.fr/promo7/fkong/',
            'promo_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
