<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PromosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promos')->insert([
            'title' => 'promo9 ',
            'startDate' => Carbon::create(2018, 12, 17),
            'endDate' => Carbon::create(2019, 6, 11),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('promos')->insert([
            'title' => 'promo7',
            'startDate' => Carbon::create(2017, 12, 10),
            'endDate' => Carbon::create(2018, 6, 5),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
