<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// List of all promos with their linked students.
Route::get('promos', 'PromoController@index');

// List of students
Route::get('students', 'StudentsController@get');

// Get/Show one particular student by id.
Route::get('/api/student/{id}', function ($id) {
    return App\Student::findOrFail($id);
});


// Create/add new student.
// Route::post('student' 'StudentController@store)

// Delete a student.
// Route::dellete('student' 'StudentController@destroy)


