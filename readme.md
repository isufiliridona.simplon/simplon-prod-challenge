# Simplon Prod Challenge


This is a challenge-project from Simplon Prod Company made with Laravel Framework. Its purpose is to 
estimate my skills in PHP.


#### I had to create a mini api REST. Here are some specifications :
- Install the framework
- Create a model named "Promo" and his migration. A "Promo" is composed by a "title", "start date" and "end date".
- Create another model named "Student" also his migration. One "Student" is composed by a "name", "surname", "email", and an external "url".
- One "Promo" can be linked to an undefined number of "Students" while one "Student" can be linked only to one "Promo".
- Make a route to get all the "Promos" with their corresponding students.


###### Bonus Functionalities :
- Create some Fixtures to furfill your SQL tables.
- Create a route to Add a new Student.


###### Deadline :
One Week.


#### How do I use your project  ?

* Step 1:
  Clone the project and install the composer:

  $composer install

* Step 2:
  Create a database named laravel_challenge.
  Set up your database and modify the .env file in the project like this:

  DB_DATABASE=laravel_challenge
  
  DB_USERNAME=your username
  
  DB_PASSWORD=your secret password


* Step 3:
  Tap these lines of code to get migrations and the seeders.

  php artisan migrate
  
  php artisan db:seed --class=PromosTableSeeder
  
  php artisan db:seed --class=StudentsTableSeeder

* Step 4:
  Run the server with the following command below :

  $php artisan serve

* Step 5:
  Test the routes :

  The route to see all "Promos" and their linked "Students"  :
  http://127.0.0.1:8000 (..or whatever port is open to you) + /promos

  The route te see all "Students" only :
  http://127.0.0.1:8000/students
  
  The route te see one particular student:
  http://127.0.0.1:8000/api/student/id

  
